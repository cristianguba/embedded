import RPi.GPIO as GPIO #Import Raspberry Pi GPIO library
from time import sleep  #Import the sleep function from the time module

GPIO.setwarnings(False) #Ignore warning for now
GPIO.setmode(GPIO.BOARD) #Use physical pin numbering
GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW) #Set pin 8 to be an output pin and set initial value to off
GPIO.setup(10, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(12, GPIO.OUT, initial=GPIO.LOW)

while True:
    color = input("Insert Collor[Red,Yellow,Green] To turn off type [OFF]  ")
    if color == "Green":
        GPIO.output(8, GPIO.HIGH) #Turn on
        GPIO.output(10, GPIO.LOW)
        GPIO.output(12, GPIO.LOW)
    if color == "Red":
        GPIO.output(8, GPIO.LOW) #Turn off
        GPIO.output(10, GPIO.HIGH)
        GPIO.output(12, GPIO.LOW)
    if color == "Yellow":
        GPIO.output(8, GPIO.LOW) #Turn off
        GPIO.output(10, GPIO.LOW)
        GPIO.output(12, GPIO.HIGH)
    if color == "OFF":
        GPIO.output(8, GPIO.LOW) #Turn off
        GPIO.output(10, GPIO.LOW)
        GPIO.output(12, GPIO.LOW)