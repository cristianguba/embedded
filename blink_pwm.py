import RPi.GPIO as IO  # Use Rpi>GPIO library to acces pins

import time  # Use time library for sleep function

IO.setwarnings(False)  # Do not show any warnings

IO.setmode(IO.BCM)  # We are programing the GPIO by BCM pin numbers

IO.setup(19, IO.OUT)  # initialize GPIO19 as an output.

p = IO.PWM(19, 100)  # GPIO19 as PWM output, with 100Hz frequency

p.start(0)  # generate PWM signal with 0% duty cycle

while True:  # infinite loop
    p.ChangeDutyCycle(100)  # change duty cycle for 100 the brightness of LED.
    time.sleep(1)  # doing that for one second
    p.ChangeDutyCycle(0)  # cycle is 0 so no light from the led
    time.sleep(1)  # wait one second
